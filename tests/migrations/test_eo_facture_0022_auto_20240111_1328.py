# barbacompta - invoicing for dummies
# Copyright (C) Entr'ouvert

import datetime


def test_migration(migration):
    old_apps = migration.before([('eo_facture', '0020_facture_accounting_year')])
    User = old_apps.get_model('auth', 'User')
    Client = old_apps.get_model('eo_facture', 'Client')
    Facture = old_apps.get_model('eo_facture', 'Facture')

    user = User.objects.create(username='user')

    client = Client.objects.create(nom='client')
    facture1 = Facture.objects.create(
        proforma=False,
        client=client,
        emission=datetime.date(2020, 1, 1),
        echeance=datetime.date(2020, 1, 16),
        creator=user,
    )

    facture2 = Facture.objects.create(
        proforma=False,
        client=client,
        emission=datetime.date(2020, 1, 1),
        echeance=datetime.date(2020, 1, 16),
        account_on_previous_period=True,
        creator=user,
    )

    new_apps = migration.apply([('eo_facture', '0022_auto_20240111_1328')])

    Facture = new_apps.get_model('eo_facture', 'Facture')
    facture1 = Facture.objects.get(pk=facture1.pk)
    facture2 = Facture.objects.get(pk=facture2.pk)
    assert facture1.accounting_year == 2020
    assert facture2.accounting_year == 2019
