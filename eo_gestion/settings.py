# barbacompta - invoicing for dummies
# Copyright (C) 2010-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path

from django.conf import global_settings

# Django settings for facturation project.

BASE_DIR = os.path.dirname(__file__)

DEBUG = True

# See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'barbacompta',
        'USER': '',  # Not used with sqlite3.
        'PASSWORD': '',  # Not used with sqlite3.
        'HOST': '',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',  # Set to empty string for default. Not used with sqlite3.
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'cache',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s %(name)s.%(funcName)s: %(message)s',
            'datefmt': '%Y-%m-%d %a %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'barbacompta': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        'factur-x': {
            'level': 'WARNING',
            'handlers': [],
            'propagate': False,
        },
    },
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Use timezone aware datetime
USE_TZ = True

LOCALE_PATHS = (os.path.join(BASE_DIR, 'eo_gestion', 'locale'),)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
STATICFILES_FINDERS = list(global_settings.STATICFILES_FINDERS) + ['gadjo.finders.XStaticFinder']


# Make this unique, and don't share it with anybody.
SECRET_KEY = '5w+ifr2ho!#x06q7dshr08wd#gt0wwp@wvbvw33kmtb+x$(9ts'

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'eo_gestion.urls'

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(os.path.dirname(__file__), 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'eo_gestion.eo_facture',
    'eo_gestion.eo_banque',
    'eo_gestion.eo_conges',
    'eo_gestion.eo_redmine',
    'eo_gestion.chorus',
    'taggit',
    'adminsortable2',
    'gadjo',
    'xstatic.pkg.select2',
)

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',)

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

LOGIN_REDIRECT_URL = '/'

# timeout used in python-requests call, in seconds
# timeout just after web server, which is usually 30s,
REQUESTS_TIMEOUT = 32

TAGGIT_CASE_INSENSITIVE = True

REDMINE_BASE_URL = 'https://dev.entrouvert.org/'
REDMINE_API_KEY = ''

PUBLIK_CONGES_APICLIENT_USER = 'barbacompta'
PUBLIK_CONGES_APICLIENT_KEY = ''
PUBLIK_CONGES_API_TIMEOUT = 15
PUBLIK_CONGES_WCS_BASE_URL = 'https://formulaires-www.entrouvert.com'
# Expected vacation card fields are :
# - type (str) : One of 'Plusieurs jours', 'Un jour', 'Un matin', 'Un après-midi'
# - debut (str: ISO date | None) : First day (included) of vacation
#                                  ( None if type != 'Plusieurs jours')
# - fin (str: ISO date | None) : Last day (included) of vacation
#                                (None if type != 'Plusieurs jours')
# - date (str: ISO date | None) : Day of vacation (None if type == 'Plusieurs jours')
#
PUBLIK_CONGES_CARD_SLUG = 'conges'
# Expected day off card fields are :
# - date_off (str : ISO date) : First day off occurence
# - date_fin (str : ISO date | None) : When set indicate that this day off will not be
#                                      taken into account after this date
# - demi_jour_off (bool) : If True indicate this is an halfday off, False indicate a
#                          full day off
PUBLIK_CONGES_DAYOFF_CARD_SLUG = 'jour-off'
# Expected card fields are :
# - ignore (bool) : True if user must be ignored
# - matricule (int) : User numeric ID
# - parisien (bool) : True is the worker lives in Paris
PUBLIK_CONGES_WORKER_CARD_SLUG = 'informations-salariales'


local_settings_file = os.environ.get('BARBACOMPTA_SETTINGS_FILE', 'local_settings.py')
if os.path.exists(local_settings_file):
    with open(local_settings_file) as fd:
        exec(fd.read())
