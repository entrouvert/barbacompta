# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging

import django
from django.core.cache import cache
from django.core.management import call_command
from uwsgidecorators import cron, timer

django.setup()

logger = logging.getLogger('barbacompta')


@timer(3600 * 3)
def update_cache(num):
    from . import decorators
    from .eo_banque.templatetags import eo_banque  # noqa pylint: disable=unused-import
    from .eo_facture.templatetags import eo_facture  # noqa pylint: disable=unused-import

    logger.debug('updating cache')
    cache.clear()
    for func in decorators.cached_func:
        try:
            func.recompute()
        except Exception:  # pylint: disable=broad-exception-caught
            logger.exception('failed to update cache for %s', func.__name__)
        else:
            logger.debug('updated cache for %s.%s', func.__module__, func.__name__)


@cron(0, 2, -1, -1, -1)
def update_structures(num):
    from eo_gestion.chorus.utils import update_structures

    update_structures()


@cron(0, -1, -1, -1, -1)
def update_projects(num):
    try:
        call_command('update-projects')
    except Exception:  # pylint: disable=broad-exception-caught
        logger.exception('failed to update-projects')
