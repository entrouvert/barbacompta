from django.contrib import admin

import eo_gestion.admin

from . import models


class ProjectAdmin(admin.ModelAdmin):
    readonly_fields = ['name', 'cpfs']
    list_display = ['name', 'get_clients', 'get_cpfs_as_text']
    list_filter = ['cpfs', 'clients']
    autocomplete_fields = ['clients']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related('cpfs')

    @admin.display(description='Clients')
    def get_clients(self, obj):
        return ', '.join(map(str, obj.clients.all()))

    @admin.display(description='CPFs')
    def get_cpfs_as_text(self, obj):
        return ', '.join([str(x) for x in obj.cpfs.all()])


admin.site.register(models.Project, ProjectAdmin)
eo_gestion.admin.site.register(models.Project, ProjectAdmin)
