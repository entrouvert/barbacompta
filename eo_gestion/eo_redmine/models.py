from django.contrib.auth.models import User
from django.db import models

from eo_gestion.eo_facture.models import Client


class Project(models.Model):
    name = models.CharField(max_length=255)
    cpfs = models.ManyToManyField(User)
    clients = models.ManyToManyField(Client, related_name='project')

    def __str__(self):
        return self.name
