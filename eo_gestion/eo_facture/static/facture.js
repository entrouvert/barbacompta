$(function () {
    console.log('facture.js loading...');
    $('[data-depends-on]').each(function (i, elt) {
        const depends_on_name = $(elt).data('depends-on');
        const $widget = $('[name=' + depends_on_name + ']');
        const $form_row = $(elt).parents('.form-row');
        function update_visibility() {
            const current_value = $widget.val();
            const state = current_value != '';
            $form_row.toggle(state);
            $form_row.find('label').toggleClass('required', state);
            $(elt).attr('required', state);
            $(elt).attr('disabled', ! state); // prevent current value from being submitted
        }
        update_visibility();
        $widget.on('change', update_visibility);
    });
    console.log('facture.js loaded.');
})
