import calendar
import dataclasses
import datetime
import json
from collections import defaultdict

import requests
from django.conf import settings
from django.http import Http404
from django.urls import reverse
from django.utils.formats import date_format
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic.dates import MonthMixin, YearMixin


class ReportError(Exception):
    pass


@dataclasses.dataclass
class DayOff:
    start: datetime.date
    end: datetime.date = None
    half: bool = False


@dataclasses.dataclass
class Holiday(DayOff):
    def __str__(self):
        if self.half:
            return '0,5j le %s' % date_format(self.start, 'd/m')
        elif self.start == self.end:
            return 'le %s' % date_format(self.start, 'd/m')
        elif (self.end - self.start).days == 1:
            return 'les %s et %s' % (date_format(self.start, 'd'), date_format(self.end, 'd/m'))

        return 'du %s au %s' % (date_format(self.start, 'd'), date_format(self.end, 'd/m'))


class WorkersMonthlyReportRedirect(RedirectView):
    pattern_name = 'admin:conges-workers-monthly-report'

    def get_redirect_url(self, *args, **kwargs):
        today = datetime.date.today()
        kwargs['year'] = today.year
        kwargs['month'] = today.month
        return super().get_redirect_url(*args, **kwargs)


workers_monthly_report_redirect = WorkersMonthlyReportRedirect.as_view()


class WorkersMonthlyReport(MonthMixin, YearMixin, TemplateView):
    template_name = 'eo_conges/workers_monthly_report.html'

    def dispatch(self, *args, **kwargs):
        try:
            self.date = datetime.date(self.get_year(), self.get_month(), 1)
        except ValueError:
            raise Http404()

        self.last_date = (self.date + datetime.timedelta(days=40)).replace(day=1) - datetime.timedelta(days=1)

        return super().dispatch(*args, **kwargs)

    def get_previous_month_url(self):
        previous_month = self.date - datetime.timedelta(days=1)
        return reverse(
            'admin:conges-workers-monthly-report',
            kwargs={'year': previous_month.year, 'month': previous_month.month},
        )

    def get_next_month_url(self):
        next_month = self.last_date + datetime.timedelta(days=1)
        return reverse(
            'admin:conges-workers-monthly-report', kwargs={'year': next_month.year, 'month': next_month.month}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['errors'] = self.errors = []
        try:
            context['workers'] = self.build_workers_data()
        except ReportError as e:
            context['fatal_error'] = e

        return context

    @staticmethod
    def raise_on_api_error(resp, error):
        data = None
        try:
            data = resp.json()
        except json.JSONDecodeError:
            pass

        if not data:
            raise ReportError('%s (%d) pas de JSON renvoyé' % (error, resp.status_code))
        if data.get('err', 0) != 0:
            raise ReportError(
                '%s (%d) %s : %s'
                % (
                    error,
                    resp.status_code,
                    data.get('err_class', 'inconnue'),
                    data.get('err_desc', 'inconnue'),
                )
            )

    @staticmethod
    def get_api_url(card_slug):
        return '%s/api/cards/%s/list' % (settings.PUBLIK_CONGES_WCS_BASE_URL, card_slug)

    def get_workers(self):
        resp = requests.get(
            self.get_api_url(settings.PUBLIK_CONGES_WORKER_CARD_SLUG),
            params={'full': 'on', 'include-actions': 'off'},
            auth=(settings.PUBLIK_CONGES_APICLIENT_USER, settings.PUBLIK_CONGES_APICLIENT_KEY),
            timeout=settings.PUBLIK_CONGES_API_TIMEOUT,
        )
        self.raise_on_api_error(resp, 'Erreur lors de la récupération des données des coopérateur·ice')
        return {
            w['user']['NameID'][0]: {
                'uuid': w['user']['NameID'][0],
                'display_name': w['user']['name'],
                'is_parisian': w['fields']['parisien'],
                'ignore': w['fields']['ignore'],
                'number': w['fields']['matricule'],
            }
            for w in resp.json()['data']
        }

    def get_days_off(self):
        resp = requests.get(
            self.get_api_url(settings.PUBLIK_CONGES_DAYOFF_CARD_SLUG),
            params={
                'full': 'on',
                'include-actions': 'off',  # lighten returned content
                'order_by': 'date_off',
            },
            auth=(settings.PUBLIK_CONGES_APICLIENT_USER, settings.PUBLIK_CONGES_APICLIENT_KEY),
            timeout=settings.PUBLIK_CONGES_API_TIMEOUT,
        )
        self.raise_on_api_error(resp, 'Erreur lors de la récupération des jours off')

        cal = calendar.Calendar()
        cal.firstweekday = calendar.MONDAY
        dom = [d for d in cal.itermonthdates(self.date.year, self.date.month) if d.month == self.date.month]
        first_dom = dom[0]
        last_dom = dom[-1]

        result = defaultdict(list)
        for dayoff in resp.json()['data']:
            fields = dayoff['fields']
            user_uuid = dayoff['user']['NameID'][0]
            date_off = datetime.date.fromisoformat(fields['date_off'])
            if date_off > last_dom:
                continue
            date_end = None if not fields['date_fin'] else datetime.date.fromisoformat(fields['date_fin'])
            if date_end and date_end < first_dom:
                continue
            result[user_uuid].append(DayOff(date_off, date_end, fields['demi_jour_off']))
        return result

    def get_holidays(self):
        cal = calendar.Calendar()
        cal.firstweekday = calendar.MONDAY
        dom = [d for d in cal.itermonthdates(self.date.year, self.date.month) if d.month == self.date.month]
        first_dom = dom[0]
        last_dom = dom[-1]
        params = {
            'filter': 'pending',
            'full': 'on',
            'include-actions': 'off',  # lighten returned content
            'filter-debut': last_dom.isoformat(),
            'filter-debut-operator': 'lte',
            'filter-fin': first_dom.isoformat(),
            'filter-fin-operator': 'gte',
            'order_by': 'debut',
        }
        resp = requests.get(
            self.get_api_url(settings.PUBLIK_CONGES_CARD_SLUG),
            params=params,
            auth=(settings.PUBLIK_CONGES_APICLIENT_USER, settings.PUBLIK_CONGES_APICLIENT_KEY),
            timeout=settings.PUBLIK_CONGES_API_TIMEOUT,
        )
        self.raise_on_api_error(resp, 'Erreur lors de la récupération des congés')
        holidays = defaultdict(list)
        for event in resp.json()['data']:
            fields = event['fields']
            user_uuid = event['user']['NameID'][0]
            if fields['type_raw'].lower() == 'plusieurs jours':
                first = max(datetime.date.fromisoformat(fields['debut']), first_dom)
                last = min(datetime.date.fromisoformat(fields['fin']), last_dom)
                holidays[user_uuid].append(Holiday(first, last, False))
            else:
                current = datetime.date.fromisoformat(fields['date'])
                halfday = fields['type_raw'].lower() != 'un jour'
                holidays[user_uuid].append(Holiday(current, current, halfday))
        return holidays

    def build_workers_data(self):
        workers = self.get_workers()
        workers_days_off = self.get_days_off()
        workers_holidays = self.get_holidays()

        workers = {k: v for k, v in workers.items() if not v['ignore']}

        for uuid, worker in workers.items():
            days_off = workers_days_off.get(uuid, [])

            holidays = workers_holidays.get(uuid, [])

            halfdays_off = [day for day in days_off if day.half]
            if len(days_off) > 2 or len(days_off) == 2 and len(halfdays_off) != 2:
                self.errors.append('%s a trop de jours off.' % worker['display_name'])
            elif not days_off:
                self.errors.append("%s n'a pas de jour off." % worker['display_name'])

            worker['holidays'] = self.normalize_holidays(holidays, [d.start.weekday() for d in days_off])
        return sorted(workers.values(), key=lambda w: w['number'])

    def normalize_holidays(self, worker_holidays, days_off=None):
        def split_holidays_on_day_off(holiday):
            start, end = holiday.start, holiday.end
            if start == end:
                if start.weekday() not in days_off:
                    yield holiday
                elif len(days_off) == 2:
                    holiday.half = True
                    yield holiday
                return

            if len(days_off) == 1 and start.weekday() == days_off[0]:
                start += datetime.timedelta(days=1)

            interval_start = start
            for i in range((end - start).days + 1):
                interval_end = start + datetime.timedelta(days=i)
                if interval_end.weekday() in days_off:
                    yield Holiday(interval_start, interval_end - datetime.timedelta(days=1))
                    if len(days_off) == 2:
                        yield Holiday(interval_end, interval_end, half=True)
                    interval_start = interval_end + datetime.timedelta(days=1)

            if interval_start <= interval_end:
                yield Holiday(interval_start, interval_end)

        def strip_weekend(holiday):
            if holiday.start.weekday() in (5, 6):
                holiday.start += datetime.timedelta(days=7 - holiday.start.weekday())
            if holiday.end.weekday() in (5, 6):
                holiday.end -= datetime.timedelta(days=holiday.end.weekday() - 4)
            if holiday.start <= holiday.end:
                return holiday

        if days_off is not None:
            worker_holidays = [
                sub_holiday
                for holiday in worker_holidays
                for sub_holiday in split_holidays_on_day_off(holiday)
            ]

        worker_holidays = [
            holiday_no_weekend
            for holiday in worker_holidays
            if (holiday_no_weekend := strip_weekend(holiday))
        ]

        return worker_holidays


workers_monthly_report = WorkersMonthlyReport.as_view()
