from datetime import date

from django.core.management.base import BaseCommand
from django.db import transaction

from eo_gestion.eo_banque.models import SoldeBanquePop


class Command(BaseCommand):
    """
    Charge un fichier CSV exporté depuis notre banque en ligne Baneque Populaire
    """

    can_import_django_settings = True
    output_transaction = True
    requires_system_checks = '__all__'
    args = '<csv_file> <csv_file>...'
    help = 'Charge le solde courant'

    @transaction.atomic
    def handle(self, compte, montant, **options):
        try:
            s = SoldeBanquePop.objects.get(compte=compte, date=date.today())
            s.montant = montant
            s.save()
        except SoldeBanquePop.DoesNotExist:
            SoldeBanquePop(compte=compte, date=date.today(), montant=montant).save()
