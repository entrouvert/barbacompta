# Configuration for barbacompta
# You can override barbacompta default settings here
# For more information on settings see
# https://docs.djangoproject.com/en/1.7/topics/settings/
# WARNING! Quick-start development settings unsuitable for production!
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

import glob
import os

# Set default path
VAR_DIR = '/var/lib/barbacompta'
STATIC_ROOT = os.path.join(VAR_DIR, 'collected-static')
MEDIA_ROOT = os.path.join(VAR_DIR, 'media')
ETC_DIR = '/etc/barbacompta/'

with open('/etc/barbacompta/secret') as fd:
    SECRET_KEY = fd.read()

DEBUG = False

if os.path.exists('/etc/barbacompta/idp-metadata.xml'):
    INSTALLED_APPS += ('mellon',)
    AUTHENTICATION_BACKENDS += ('mellon.backends.SAMLBackend',)

    MELLON_ATTRIBUTE_MAPPING = {
        'username': '{attributes[username][0]}',
        'email': '{attributes[email][0]}',
        'first_name': '{attributes[first_name][0]}',
        'last_name': '{attributes[last_name][0]}',
    }
    MELLON_SUPERUSER_MAPPING = {'is_superuser': ('true',)}
    MELLON_USERNAME_TEMPLATE = '{attributes[username][0]}'
    MELLON_PUBLIC_KEYS = ['/etc/barbacompta/saml.crt']
    MELLON_PRIVATE_KEY = '/etc/barbacompta/saml.key'
    MELLON_IDENTITY_PROVIDERS = [{'METADATA': '/etc/barbacompta/idp-metadata.xml'}]

for filename in sorted(glob.glob(os.path.join(ETC_DIR, 'settings.d', '*.py'))):
    exec(open(filename).read())
