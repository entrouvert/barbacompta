# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0004_contrat_public_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='contrat',
            name='image',
            field=models.ImageField(upload_to='images/', null=True, blank=True, verbose_name='Image'),
        ),
    ]
