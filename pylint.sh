#!/bin/bash
set -e

pylint --jobs ${NUMPROCESSES:-1} -f parseable --rcfile pylint.rc "$@" | tee pylint.out; test $PIPESTATUS -eq 0
