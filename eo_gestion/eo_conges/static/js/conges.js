const button = document.getElementById('copy-report');

button.addEventListener("click", (event) => {
  var text = '';
  document.querySelectorAll('.worker').forEach(el => {
    el.querySelectorAll('span').forEach(el => {
      text += el.textContent + '\n';
    })
    text += '\n';
  })
  navigator.clipboard.writeText(text.trim());
  button.innerHTML = 'Copié !';
  button.removeAttribute('href');
});
