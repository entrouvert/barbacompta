from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Commentaire',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('contenu', models.TextField()),
                ('object_id', models.PositiveIntegerField()),
                ('creation', models.DateTimeField(auto_now_add=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LigneBanquePop',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('compte', models.CharField(max_length=20)),
                ('date_comptabilisation', models.DateField()),
                ('date_operation', models.DateField()),
                ('libelle', models.TextField(max_length=256)),
                ('reference', models.CharField(max_length=20)),
                ('date_valeur', models.DateField(db_index=True)),
                ('montant', models.DecimalField(max_digits=10, decimal_places=2, db_index=True)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SoldeBanquePop',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('compte', models.CharField(max_length=20)),
                ('date', models.DateField(auto_now_add=True)),
                ('montant', models.DecimalField(max_digits=10, decimal_places=2)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='lignebanquepop',
            unique_together={
                ('compte', 'date_comptabilisation', 'date_operation', 'libelle', 'reference', 'montant')
            },
        ),
    ]
