# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest


@pytest.fixture
def homepage(app):
    response = app.get('/').follow()

    response.form.set('username', 'admin')
    response.form.set('password', 'admin')

    return response.form.submit().follow()


def test_misc(homepage):
    clients = homepage.click('Clients')
    str(clients)
    ajouter_un_client = homepage.click('Ajouter un client')
    str(ajouter_un_client)
    contrats = homepage.click('Contrats')
    contrat_2c210afd24c11596eeaf94bfb = contrats.click('2c210afd24c11596eeaf94bfb', href='change')
    str(contrat_2c210afd24c11596eeaf94bfb)
    ajouter_un_contrat = homepage.click('Ajouter un contrat')
    str(ajouter_un_contrat)
    compte_en_banque = homepage.click('Compte en banque')
    str(compte_en_banque)
    factures = homepage.click('Factures')
    factures_00137 = factures.click('F20190137')
    str(factures_00137)
    rapid = factures.click('Rapid')
    str(rapid)


def test_ods_export(app):
    response = app.get('/eo_facture/facture/').follow()

    response.form.set('username', 'admin')
    response.form.set('password', 'admin')

    factures = response.form.submit().follow()

    response = factures.click('Export ODS')
    assert 'factures.ods' in response['Content-Disposition']


def test_odt_export(app):
    response = app.get('/eo_facture/contrat/').follow()
    response.form.set('username', 'admin')
    response.form.set('password', 'admin')

    contracts = response.form.submit().follow()
    form = contracts.forms['changelist-form']
    for i in range(5):
        form.set('_selected_action', True, index=i)
    form['action'] = 'export_references_as_fodt'
    res = form.submit('Go')
    assert res.status_code == 200
    assert res.content_type == 'application/vnd.oasis.opendocument.text'


def test_zip_export(app):
    response = app.get('/eo_facture/facture/').follow()
    response.form.set('username', 'admin')
    response.form.set('password', 'admin')

    factures = response.form.submit().follow()
    form = factures.forms['changelist-form']
    for i in range(5):
        form.set('_selected_action', True, index=i)
    form['action'] = 'export_invoices_as_zip'
    res = form.submit('Go')
    assert res.status_code == 200
    assert res.content_type == 'application/zip'


def test_previsionnel_par_client(homepage):
    table = ''
    for tr in homepage.pyquery('table#table-previsional-income-by-client-2019 tr').items():
        for td in tr.find('td').items():
            table += td.text() + '\n'
        table += '\n'
    assert (
        table
        == '''\
39330e7522883a
225 587,42
183 343,22
81,27 %
42 244,20
18,73 %
8,15 %
8,15

20a8226446
189 020,82
180 020,82
95,24 %
9 000,00
4,76 %
6,83 %
14,97

b70305f
132 675,00
129 725,00
97,78 %
2 950,00
2,22 %
4,79 %
19,77

65fe9bf71bff6963c7d0ac6dd3c7019
131 000,00
131 000,00
100,00 %
0,00
0,00 %
4,73 %
24,50

3e44a97d7946a958ebbf696ab54c
88 750,00
88 750,00
100,00 %
0,00
0,00 %
3,21 %
27,70

1562966
75 293,75
31 600,00
41,97 %
43 693,75
58,03 %
2,72 %
30,42

462a
71 212,50
64 150,00
90,08 %
7 062,50
9,92 %
2,57 %
32,99

c85fb096c1da3d8
70 975,50
70 975,50
100,00 %
0,00
0,00 %
2,56 %
35,56

1d7aa901995f387c568b241ec9ff3b9
65 850,00
65 850,00
100,00 %
0,00
0,00 %
2,38 %
37,93

d384f2a467ea1ba1017428a0576e62d9
60 040,00
60 040,00
100,00 %
0,00
0,00 %
2,17 %
40,10

d5381e9ca259c32f4ae7abbf3ed61899
56 641,66
56 641,66
100,00 %
0,00
0,00 %
2,05 %
42,15

d76cfe37
52 168,50
51 723,75
99,15 %
444,75
0,85 %
1,88 %
44,03

c56e9
50 022,60
50 022,60
100,00 %
0,00
0,00 %
1,81 %
45,84

b5be1e99
50 000,00
50 000,00
100,00 %
0,00
0,00 %
1,81 %
47,65

a2fc8232d6d8f180e24c323b0cfdf
49 700,00
43 866,67
88,26 %
5 833,33
11,74 %
1,79 %
49,44

452f3d5ed41b3014fcf146d8c
49 682,50
49 682,50
100,00 %
0,00
0,00 %
1,79 %
51,23

c1ceda049dc
48 926,85
48 926,85
100,00 %
0,00
0,00 %
1,77 %
53,00

cda12ba676314a6f47402aa83b1ed830
47 957,50
39 650,00
82,68 %
8 307,50
17,32 %
1,73 %
54,73

c0d3570ac1dd16122
45 024,00
45 024,00
100,00 %
0,00
0,00 %
1,63 %
56,36

e2858d3c9294aca375a348223a45f4a6
44 800,00
44 800,00
100,00 %
0,00
0,00 %
1,62 %
57,98

862d8ff3b51af23d64285c43
43 078,00
43 078,00
100,00 %
0,00
0,00 %
1,56 %
59,53

097b81431de83d2574751e1
42 450,00
42 450,00
100,00 %
0,00
0,00 %
1,53 %
61,07

b86f135514068ee335
40 962,00
40 962,00
100,00 %
0,00
0,00 %
1,48 %
62,55

4678b4
36 618,55
36 618,55
100,00 %
0,00
0,00 %
1,32 %
63,87

d8d2821cf1099d
34 162,50
33 775,00
98,87 %
387,50
1,13 %
1,23 %
65,10

25f81
33 670,00
31 670,00
94,06 %
2 000,00
5,94 %
1,22 %
66,32

0ae83ac8
33 117,00
32 977,00
99,58 %
140,00
0,42 %
1,20 %
67,51

11ac1ba60a1360c6203d1ab0f127f8c
30 850,00
30 850,00
100,00 %
0,00
0,00 %
1,11 %
68,63

2d231fc85753765a0e
29 467,23
27 220,51
92,38 %
2 246,72
7,62 %
1,06 %
69,69

8ddc
28 100,00
28 100,00
100,00 %
0,00
0,00 %
1,01 %
70,71

71fddac221466eabc717114a
27 800,00
24 725,00
88,94 %
3 075,00
11,06 %
1,00 %
71,71

e9c25
26 948,60
26 948,60
100,00 %
0,00
0,00 %
0,97 %
72,68

20539332989c51
26 887,50
26 887,50
100,00 %
0,00
0,00 %
0,97 %
73,66

bf6c03cf2f40c
26 022,00
26 022,00
100,00 %
0,00
0,00 %
0,94 %
74,60

0b1b26cacc602dff7219586676ad0f5d
25 712,50
25 712,50
100,00 %
0,00
0,00 %
0,93 %
75,52

606f0f602285ede7
25 025,00
25 025,00
100,00 %
0,00
0,00 %
0,90 %
76,43

d59aa5f7223458e5b33eb29a696
24 960,00
24 960,00
100,00 %
0,00
0,00 %
0,90 %
77,33

20ac5dd97b21b6ef0655d32
24 150,00
23 587,50
97,67 %
562,50
2,33 %
0,87 %
78,20

3c4c0ae8165b90b7345e21f0792c
23 930,00
10 430,00
43,59 %
13 500,00
56,41 %
0,86 %
79,07

dac8bff8ffc95f131ac0dafcdfb36
21 900,00
21 900,00
100,00 %
0,00
0,00 %
0,79 %
79,86

ab41680aef75
21 700,00
0,00
0,00 %
21 700,00
100,00 %
0,78 %
80,64

dad006dc0bd5
21 502,00
16 510,00
76,78 %
4 992,00
23,22 %
0,78 %
81,42

2c210afd24c11596eeaf94bfb
21 500,00
21 500,00
100,00 %
0,00
0,00 %
0,78 %
82,19

9b6b4
21 455,00
21 455,00
100,00 %
0,00
0,00 %
0,77 %
82,97

f98dd57ae4ac158c7
19 584,10
16 801,35
85,79 %
2 782,75
14,21 %
0,71 %
83,68

b5367a456
18 939,00
18 939,00
100,00 %
0,00
0,00 %
0,68 %
84,36

0031c
18 705,67
18 705,67
100,00 %
0,00
0,00 %
0,68 %
85,04

df2caae86e4be66bcc07
18 275,00
18 275,00
100,00 %
0,00
0,00 %
0,66 %
85,70

24d5
17 500,00
14 000,00
80,00 %
3 500,00
20,00 %
0,63 %
86,33

c39a42e4d
17 220,00
17 000,00
98,72 %
220,00
1,28 %
0,62 %
86,95

3a1512781a0a53
16 900,00
16 900,00
100,00 %
0,00
0,00 %
0,61 %
87,56

8194d71612a5a4b5bfdf91d582327764
16 375,00
11 325,00
69,16 %
5 050,00
30,84 %
0,59 %
88,15

687d
16 000,00
16 000,00
100,00 %
0,00
0,00 %
0,58 %
88,73

9c64b7cf63f9776
15 530,00
15 530,00
100,00 %
0,00
0,00 %
0,56 %
89,29

145a75400ea1fdea919b4bb
15 500,00
15 500,00
100,00 %
0,00
0,00 %
0,56 %
89,85

212f2acf453fb5a7dc
14 350,00
11 850,00
82,58 %
2 500,00
17,42 %
0,52 %
90,37

a7d5
13 100,00
13 100,00
100,00 %
0,00
0,00 %
0,47 %
90,84

859599
13 000,00
13 000,00
100,00 %
0,00
0,00 %
0,47 %
91,31

94e3b765f0ef1a6321eb2b508400
12 880,00
12 880,00
100,00 %
0,00
0,00 %
0,47 %
91,78

b4a8eec84
12 090,00
12 090,00
100,00 %
0,00
0,00 %
0,44 %
92,21

4ba22
12 000,00
12 000,00
100,00 %
0,00
0,00 %
0,43 %
92,65

3f77270a86964516d7625644
12 000,00
12 000,00
100,00 %
0,00
0,00 %
0,43 %
93,08

a5698cdc6933401679600275e748347
11 900,00
11 900,00
100,00 %
0,00
0,00 %
0,43 %
93,51

f291cca
11 620,00
11 620,00
100,00 %
0,00
0,00 %
0,42 %
93,93

071d6d5d4360a12dbba47d8
11 000,00
11 000,00
100,00 %
0,00
0,00 %
0,40 %
94,33

4224ed042b1ce6fb174187
10 925,00
10 925,00
100,00 %
0,00
0,00 %
0,39 %
94,72

e8b13
10 700,00
10 700,00
100,00 %
0,00
0,00 %
0,39 %
95,11

c1418af1
10 500,00
10 500,00
100,00 %
0,00
0,00 %
0,38 %
95,49

537970
9 600,00
9 600,00
100,00 %
0,00
0,00 %
0,35 %
95,83

c86e6f2c0dc710d9
9 305,00
6 000,00
64,48 %
3 305,00
35,52 %
0,34 %
96,17

17a589db260721da2cf236d767fd
9 100,00
9 100,00
100,00 %
0,00
0,00 %
0,33 %
96,50

c3f42bb0d75d379
9 098,50
6 100,85
67,05 %
2 997,65
32,95 %
0,33 %
96,83

c3fa6dd02523abaab4e28b6e0cd
9 000,00
0,00
0,00 %
9 000,00
100,00 %
0,33 %
97,15

060d0
8 333,00
7 283,00
87,40 %
1 050,00
12,60 %
0,30 %
97,45

6e9dec375
7 800,00
7 800,00
100,00 %
0,00
0,00 %
0,28 %
97,73

d03745646eedce1389c895c7b
6 500,00
6 500,00
100,00 %
0,00
0,00 %
0,23 %
97,97

6905d1362215cf
6 250,00
6 250,00
100,00 %
0,00
0,00 %
0,23 %
98,19

0973e3a7ebd843d1
6 050,00
6 050,00
100,00 %
0,00
0,00 %
0,22 %
98,41

c7d7be8d5badfec12747fd96
5 295,00
5 295,00
100,00 %
0,00
0,00 %
0,19 %
98,60

a511e6
5 250,00
5 250,00
100,00 %
0,00
0,00 %
0,19 %
98,79

4b09a7c766dad6d6c3c85
5 000,00
5 000,00
100,00 %
0,00
0,00 %
0,18 %
98,97

16812c10bf8bd5e5c5f8eb6e8249dad7
4 017,50
0,00
0,00 %
4 017,50
100,00 %
0,15 %
99,12

f66253e5a
3 650,00
3 650,00
100,00 %
0,00
0,00 %
0,13 %
99,25

73fd3cf7de
3 500,00
3 500,00
100,00 %
0,00
0,00 %
0,13 %
99,38

55c1c1ce4c022b4b13
3 270,00
3 270,00
100,00 %
0,00
0,00 %
0,12 %
99,50

c16b09ce5ff9d6b4
3 025,00
3 025,00
100,00 %
0,00
0,00 %
0,11 %
99,60

62da650c65d031a7db5f0
3 000,00
3 000,00
100,00 %
0,00
0,00 %
0,11 %
99,71

61b30ef8276e4d851b7
2 750,00
0,00
0,00 %
2 750,00
100,00 %
0,10 %
99,81

eb717ca47398b60ed80e776f0a49e1b0
2 700,00
2 700,00
100,00 %
0,00
0,00 %
0,10 %
99,91

2a2bf548
2 500,00
2 500,00
100,00 %
0,00
0,00 %
0,09 %
100,00

Total
2 768 884,25
2 563 571,60
92,59 %
205 312,65
7,41 %
100,00 %


'''
    )
