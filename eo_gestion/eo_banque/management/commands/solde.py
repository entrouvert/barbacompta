from datetime import date, datetime, timedelta

from django.core.management.base import BaseCommand
from django.db import transaction

from eo_gestion.eo_banque.models import solde


class Command(BaseCommand):
    """
    Charge un fichier CSV exporté depuis notre banque en ligne Banque Populaire
    """

    can_import_django_settings = True
    output_transaction = True
    requires_system_checks = '__all__'
    args = '<csv_file> <csv_file>...'
    help = 'Charge le solde courant'

    @transaction.atomic
    def handle(self, *args, **options):
        if args:
            now = datetime.strptime(args[0], '%Y%m%d').date()
        else:
            now = date.today() + timedelta(days=1)
        print('Solde', solde(now))
