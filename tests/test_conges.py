import base64
import contextlib
import dataclasses
import datetime

import responses
from django.utils.html import escape

from .utils import login

WORKERS_URL = 'https://formulaires-www.entrouvert.com/api/cards/informations-salariales/list'
DAYSOFF_URL = 'https://formulaires-www.entrouvert.com/api/cards/jour-off/list'
HOLIDAYS_URL = 'https://formulaires-www.entrouvert.com/api/cards/conges/list'

WORKERS_RESPONSE = [
    {
        'fields': {'ignore': False, 'matricule': 42, 'parisien': False},
        'user': {'NameID': ['Jean'], 'name': 'Jean Dupont'},
    },
]


@dataclasses.dataclass
class DayOff:
    name_id: str
    start: datetime.date
    half: bool = False

    def to_json(self):
        return {
            'user': {'NameID': [self.name_id]},
            'fields': {
                'demi_jour_off': self.half,
                'date_off': self.start.isoformat(),
                'date_fin': None,
            },
        }


@dataclasses.dataclass
class Holiday:
    name_id: str
    start: datetime.date
    end: datetime.date = None
    half: bool = False

    def to_json(self):
        if self.half:
            type_raw = 'Un Matin'
        elif self.end:
            type_raw = 'Plusieurs Jours'
        else:
            type_raw = 'Un jour'

        return {
            'user': {'NameID': [self.name_id]},
            'fields': {
                'date': self.start.isoformat() if not self.end else None,
                'debut': self.start.isoformat(),
                'fin': self.end.isoformat() if self.end else self.start.isoformat(),
                'type_raw': type_raw,
            },
        }


@contextlib.contextmanager
def mock_api(holidays=None, days_off=None, year=2023, month=1, all_requests_fired=True):
    with responses.RequestsMock(assert_all_requests_are_fired=all_requests_fired) as rsps:
        # Workers list mock
        rsps.add(
            responses.GET,
            WORKERS_URL,
            status=200,
            json={'data': WORKERS_RESPONSE},
        )

        # Days off mocks
        rsps.add(
            responses.GET,
            DAYSOFF_URL,
            json={'data': [x.to_json() for x in days_off or []]},
            status=200,
        )

        # holidays mocks
        rsps.add(
            responses.GET,
            HOLIDAYS_URL,
            json={'data': [x.to_json() for x in holidays or []]},
            status=200,
        )
        yield rsps


def test_conges_redirect(app, settings):
    login(app)
    resp = app.get('/conges/')
    assert resp.location == '/conges/2019/1/'


def test_conges_bad_month(app, settings):
    login(app)
    app.get('/conges/2024/13/', status=404)


def test_conges(app, settings):
    settings.PUBLIK_CONGES_APICLIENT_KEY = 'xxx'

    holidays = []

    login(app)
    with mock_api(holidays=holidays, year=2023, month=1) as rsps:
        resp = app.get('/conges/2023/01/')
        calls = list(rsps.calls)

    assert [x.text for x in resp.pyquery('.worker span')] == [
        'Jean Dupont (matricule 42)',
        'Congés : 0.',
    ]

    assert '/conges/2022/12/' in resp.text
    assert '/conges/2023/2/' in resp.text

    auth_token = 'YmFyYmFjb21wdGE6eHh4'
    assert all(call.request.headers['Authorization'] == f'Basic {auth_token}' for call in calls)
    assert base64.b64decode('YmFyYmFjb21wdGE6eHh4') == b'barbacompta:xxx'

    assert calls[1].request.params == {'full': 'on', 'include-actions': 'off', 'order_by': 'date_off'}
    assert calls[2].request.params == {
        'filter': 'pending',
        'full': 'on',
        'include-actions': 'off',
        'filter-debut': '2023-01-31',
        'filter-debut-operator': 'lte',
        'filter-fin': '2023-01-01',
        'filter-fin-operator': 'gte',
        'order_by': 'debut',
    }

    holidays.append(Holiday('Jean', datetime.date(2023, 1, 2), datetime.date(2023, 1, 2)))

    with mock_api(holidays=holidays, year=2023, month=1):
        resp = app.get('/conges/2023/01/')

    assert [x.text for x in resp.pyquery('.worker span')] == [
        'Jean Dupont (matricule 42)',
        'Congés : le 02/01.',
    ]

    holidays += [
        # one day, using dates
        Holiday('Jean', datetime.date(2023, 1, 4), datetime.date(2023, 1, 4)),
        # two days range, using dates
        Holiday('Jean', datetime.date(2023, 1, 10), datetime.date(2023, 1, 11)),
        Holiday('Jean', datetime.date(2023, 1, 12), datetime.date(2023, 1, 13)),
        # 4 days range, using dates
        Holiday('Jean', datetime.date(2023, 1, 16), datetime.date(2023, 1, 19)),
        # only half day
        Holiday('Jean', datetime.date(2023, 1, 23), half=True),
        # only half day
        Holiday('Jean', datetime.date(2023, 1, 24), half=True),
    ]

    # 6 days range
    event = Holiday('Jean', datetime.date(2023, 1, 25), datetime.date(2023, 1, 31))
    holidays.append(event)

    with mock_api(holidays=holidays, year=2023, month=1):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == (
        'Congés : le 02/01, le 04/01, les 10 et 11/01, les 12 et 13/01, du 16 au 19/01, 0,5j le 23/01, '
        '0,5j le 24/01, du 25 au 31/01.'
    )


def test_conges_error(app):
    holidays = []
    some_err = {'err': 1, 'err_class': 'Exception', 'err_desc': 'une erreur'}

    login(app)
    with mock_api(holidays=holidays, all_requests_fired=False) as rsps:
        rsps.replace(responses.GET, WORKERS_URL, status=500, body='error')

        resp = app.get('/conges/2023/01/')

    assert (
        'Erreur lors de la récupération des données des coopérateur·ice (500) pas de JSON renvoyé'
        in resp.text
    )

    with mock_api(holidays=holidays, all_requests_fired=False) as rsps:
        rsps.replace(responses.GET, WORKERS_URL, status=400, json=some_err)

        resp = app.get('/conges/2023/01/')

    assert (
        'Erreur lors de la récupération des données des coopérateur·ice (400) Exception : une erreur'
        in resp.text
    )

    with mock_api(holidays=holidays, all_requests_fired=False) as rsps:
        rsps.replace(responses.GET, DAYSOFF_URL, status=400, body='error')

        resp = app.get('/conges/2023/01/')

    assert 'Erreur lors de la récupération des jours off (400) pas de JSON renvoyé' in resp.text

    with mock_api(holidays=holidays, all_requests_fired=False) as rsps:
        rsps.replace(responses.GET, DAYSOFF_URL, status=500, json=some_err)

        resp = app.get('/conges/2023/01/')

    assert 'Erreur lors de la récupération des jours off (500) Exception : une erreur' in resp.text

    with mock_api(holidays=holidays, all_requests_fired=False) as rsps:
        rsps.replace(responses.GET, HOLIDAYS_URL, status=500, json=some_err)

        resp = app.get('/conges/2023/01/')

    assert 'Erreur lors de la récupération des congés (500) Exception : une erreur' in resp.text

    with mock_api(holidays=holidays, all_requests_fired=False) as rsps:
        rsps.replace(responses.GET, HOLIDAYS_URL, status=500, body='error')

        resp = app.get('/conges/2023/01/')

    assert 'Erreur lors de la récupération des congés (500) pas de JSON renvoyé' in resp.text


def test_conges_day_off_error(app):
    holidays = []

    login(app)
    with mock_api(holidays=holidays):
        resp = app.get('/conges/2023/01/')

    assert escape("Jean Dupont n'a pas de jour off.") in resp.text

    day_off1 = DayOff('Jean', datetime.date(2023, 1, 9))
    day_off2 = DayOff('Jean', datetime.date(2023, 1, 11))
    days_off = [day_off1, day_off2]

    with mock_api(days_off=days_off):
        resp = app.get('/conges/2023/01/')

    assert 'Jean Dupont a trop de jours off.' in resp.text

    day_off1.half = day_off2.half = True

    with mock_api(days_off=days_off):
        resp = app.get('/conges/2023/01/')

    assert 'Jean Dupont a trop de jours off' not in resp.text

    days_off.append(DayOff('Jean', datetime.date(2023, 1, 12), half=True))

    with mock_api(days_off=days_off):
        resp = app.get('/conges/2023/01/')

    assert 'Jean Dupont a trop de jours off.' in resp.text


def test_conges_is_parisian(app):
    holidays = []

    login(app)
    with mock_api(holidays=holidays):
        resp = app.get('/conges/2023/01/')

    assert 'Carte transport Paris 50%' not in resp.text

    response = WORKERS_RESPONSE.copy()
    response[0]['fields']['parisien'] = True

    with mock_api(holidays=holidays) as rsps:
        rsps.replace(responses.GET, WORKERS_URL, json={'data': response})

        resp = app.get('/conges/2023/01/')

    assert 'Carte transport Paris 50%' in resp.text


def test_conges_ignored_workers(app):
    holidays = [
        Holiday('Jean', datetime.date(2023, 1, 2)),
        Holiday('Louise', datetime.date(2023, 1, 3)),
    ]

    response = WORKERS_RESPONSE.copy()
    response.append(
        {
            'fields': {'ignore': False, 'matricule': 0, 'parisien': False},
            'user': {'NameID': ['Louise'], 'name': 'Louise Michel'},
        }
    )

    login(app)
    with mock_api(holidays=holidays) as rsps:
        rsps.replace(responses.GET, WORKERS_URL, json={'data': response})

        resp = app.get('/conges/2023/01/')

    assert 'Jean' in resp.text
    assert 'Louise' in resp.text

    response[1]['fields']['ignore'] = True

    with mock_api(holidays=holidays) as rsps:
        rsps.replace(responses.GET, WORKERS_URL, json={'data': response})

        resp = app.get('/conges/2023/01/')

    assert 'Jean' in resp.text
    assert 'Louise' not in resp.text


def test_conges_strip_weekend(app):
    holidays = [
        # starts friday, ends monday
        Holiday('Jean', datetime.date(2023, 1, 6), datetime.date(2023, 1, 9)),
        # start saturday, ends sunday next week
        Holiday('Jean', datetime.date(2023, 1, 14), datetime.date(2023, 1, 22)),
        # two days of weekend
        Holiday('Jean', datetime.date(2023, 1, 28), datetime.date(2023, 1, 29)),
    ]

    login(app)
    with mock_api(holidays=holidays):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : du 06 au 09/01, du 16 au 20/01.'


def test_conges_month_span(app):
    holidays = [
        # starts in december
        Holiday('Jean', datetime.date(2022, 12, 25), datetime.date(2023, 1, 4)),
        # ends in february
        Holiday('Jean', datetime.date(2023, 1, 30), datetime.date(2023, 2, 9)),
    ]

    login(app)
    with mock_api(holidays=holidays):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : du 02 au 04/01, les 30 et 31/01.'


def test_conges_split_on_day_off(app):
    holiday = Holiday('Jean', datetime.date(2023, 1, 2), datetime.date(2023, 1, 6))

    login(app)
    with mock_api(holidays=[holiday]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : du 02 au 06/01.'

    # off on wednesday, holidays split in half
    day_off = DayOff('Jean', datetime.date(2023, 1, 4))

    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : les 02 et 03/01, les 05 et 06/01.'

    # off on friday, holidays end one day sooner
    day_off.start = datetime.date(2023, 1, 6)

    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : du 02 au 05/01.'

    # off on monday, holidays start one day later
    day_off.start = datetime.date(2023, 1, 2)

    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : du 03 au 06/01.'

    # long holidays get split multiple times
    holiday.end = datetime.date(2023, 1, 20)

    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert (
        resp.pyquery('.worker span.holidays').text()
        == 'Congés : du 03 au 06/01, du 10 au 13/01, du 17 au 20/01.'
    )

    # split on wednesday
    day_off.start = datetime.date(2023, 1, 4)

    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert (
        resp.pyquery('.worker span.holidays').text()
        == 'Congés : les 02 et 03/01, du 05 au 10/01, du 12 au 17/01, les 19 et 20/01.'
    )


def test_conges_split_on_multiple_days_off(app):
    holiday = Holiday('Jean', datetime.date(2023, 1, 2), datetime.date(2023, 1, 6))

    login(app)
    with mock_api(holidays=[holiday]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : du 02 au 06/01.'

    days_off = [
        # off on half monday
        DayOff('Jean', datetime.date(2023, 1, 2), half=True),
        # off on half wednesday
        DayOff('Jean', datetime.date(2023, 1, 11), half=True),
    ]

    with mock_api(holidays=[holiday], days_off=days_off):
        resp = app.get('/conges/2023/01/')

    assert (
        resp.pyquery('.worker span.holidays').text()
        == 'Congés : 0,5j le 02/01, le 03/01, 0,5j le 04/01, les 05 et 06/01.'
    )

    # long holidays get split multiple times
    holiday.end = datetime.date(2023, 1, 13)

    with mock_api(holidays=[holiday], days_off=days_off):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == (
        'Congés : 0,5j le 02/01, le 03/01, 0,5j le 04/01, les 05 et 06/01, 0,5j le 09/01, '
        'le 10/01, 0,5j le 11/01, les 12 et 13/01.'
    )


def test_conges_split_on_day_off_single_day(app):
    # off on wednesday
    day_off = DayOff('Jean', datetime.date(2023, 1, 4))

    # holidays on monday
    holiday = Holiday('Jean', datetime.date(2023, 1, 2), datetime.date(2023, 1, 2))

    login(app)
    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : le 02/01.'

    # off on monday
    day_off.start = datetime.date(2023, 1, 2)

    with mock_api(holidays=[holiday], days_off=[day_off]):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : 0.'


def test_conges_split_on_multiple_days_off_single_day(app):
    days_off = [
        # off on half monday
        DayOff('Jean', datetime.date(2023, 1, 2), half=True),
        # off on half wednesday
        DayOff('Jean', datetime.date(2023, 1, 4), half=True),
    ]

    # holidays on monday
    holiday = Holiday('Jean', datetime.date(2023, 1, 2))

    login(app)
    with mock_api(holidays=[holiday], days_off=days_off):
        resp = app.get('/conges/2023/01/')

    assert resp.pyquery('.worker span.holidays').text() == 'Congés : 0,5j le 02/01.'
