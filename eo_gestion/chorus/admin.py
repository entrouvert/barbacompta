# barbacompta - accounting for dummies
# Copyright (C) 2010-2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin

import eo_gestion.admin

from . import models


class StructureAdmin(admin.ModelAdmin):
    list_display = ['name', 'siret', 'service_name', 'service_code', 'engagement_obligatoire']
    search_fields = ['name', 'siret']


admin.site.register(models.Structure, StructureAdmin)
eo_gestion.admin.site.register(models.Structure, StructureAdmin)
