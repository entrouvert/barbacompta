# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django_webtest
import pytest
from django.contrib.auth import get_user_model
from django.core.management import call_command

User = get_user_model()

DATA = ['tests/fixture.json']


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker, django_db_keepdb, django_db_createdb):
    with django_db_blocker.unblock():
        if not django_db_keepdb or django_db_createdb or User.objects.count() == 0:
            for data in DATA:
                call_command('loaddata', data)
            admin, _ = User.objects.update_or_create(
                username='admin',
                defaults={'email': 'admin@example.com', 'is_superuser': True, 'is_staff': True},
            )
            admin.set_password('admin')
            admin.save()
    yield


@pytest.fixture
def app(db, freezer):
    freezer.move_to('2019-01-01')
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    try:
        return django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost'})
    finally:
        wtm._unpatch_settings()


@pytest.fixture(autouse=True)
def media(settings, tmpdir):
    settings.MEDIA_ROOT = str(tmpdir.mkdir('media'))
