# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import django.contrib.admin
from django.conf import settings
from django.urls import include, path, re_path

from . import admin
from .eo_facture.views import api_references

django.contrib.admin.autodiscover()

urlpatterns = [
    path('api/references/', api_references),
    re_path(r'^', admin.site.urls),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns = [
        path('accounts/mellon/', include('mellon.urls')),
    ] + urlpatterns
