from decimal import Decimal

import pytest
from django.core.exceptions import ValidationError

from eo_gestion.eo_facture.fields import PercentagePerYearFormField


def test_percentage_per_year():
    field = PercentagePerYearFormField()
    assert field.clean('2020:50,2021:50') == [(2020, Decimal('0.5')), (2021, Decimal('0.5'))]

    with pytest.raises(ValidationError, match=r'percentage does not sum to 100'):
        field.clean('2020:50,2021:60')

    with pytest.raises(ValidationError, match=r'percentage does not sum to 100'):
        field.clean('2020:50,2021:10')

    with pytest.raises(ValidationError, match=r'years are not ordered'):
        field.clean('2021:50,2020:50')

    with pytest.raises(ValidationError, match=r'years are not unique'):
        field.clean('2020:50,2020:50')

    with pytest.raises(ValidationError, match=r'years are not consecutive'):
        field.clean('2020:50,2022:50')
