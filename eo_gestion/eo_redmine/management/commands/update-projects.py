import requests
from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import transaction

from eo_gestion.eo_redmine.models import Project

REDMINE_HEADERS = {'X-Redmine-API-Key': settings.REDMINE_API_KEY}


def redmine_iterator(base_url, entity_name):
    result = requests.get(
        base_url + '?limit=100', headers=REDMINE_HEADERS, timeout=settings.REQUESTS_TIMEOUT
    ).json()
    total = 0
    while total < result['total_count']:
        for item in result[entity_name]:
            yield item
            total += 1
        if total < result['total_count']:
            result = requests.get(
                base_url + '?limit=100&offset=%s' % total,
                headers=REDMINE_HEADERS,
                timeout=settings.REQUESTS_TIMEOUT,
            ).json()


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **options):
        alive_projects = []
        redmine_users = {
            user['id']: user['login']
            for user in redmine_iterator(settings.REDMINE_BASE_URL + 'users.json', 'users')
        }
        barba_users = {user.username: user for user in User.objects.all()}
        projects = Project.objects.prefetch_related('cpfs', 'clients')
        projects_map = {project.id: project for project in projects}

        for project in redmine_iterator(settings.REDMINE_BASE_URL + 'projects.json', 'projects'):
            project_id = project['id']
            project_name = project['name']

            # Skip inactive or non-client projects
            if project['status'] != 1:
                continue
            if not ('parent' in project):
                continue
            if (
                project['parent']['name'] != 'Projets Clients'
                and project['parent']['id'] not in alive_projects
            ):
                continue

            # get the members
            cpf_ids = set()
            for membership in redmine_iterator(
                settings.REDMINE_BASE_URL + 'projects/%s/memberships.json' % project_id, 'memberships'
            ):
                if not 'user' in membership:
                    continue
                for role in membership['roles']:
                    if role['name'] == 'CPF':
                        cpf_ids.add(membership['user']['id'])
            cpfs = set()
            for cpf_id in cpf_ids:
                if cpf_id not in redmine_users:
                    continue
                redmine_login = redmine_users[cpf_id]

                if redmine_login not in barba_users:
                    continue
                cpfs.add(barba_users[redmine_login])

            # Require sub-projects to have a CPF defined
            if len(cpfs) == 0 and project['parent']['name'] != 'Projets Clients':
                continue

            parent_project = projects_map.get(project['parent']['id'])

            alive_projects.append(project_id)

            # now match in our DB, maybe update
            project_object = projects.filter(id=project_id, name=project_name).first()
            if not project_object:
                project_object, created = projects.update_or_create(
                    id=project_id, defaults={'name': project_name}
                )
                if created:
                    project_object.cpfs.set(cpfs)
                    if options['verbosity'] > 0:
                        print('Created project', project_name, '#', project_id)
                    continue
                if options['verbosity'] > 0:
                    print('Updated name of project', project_name, '#', project_id)

            if not project_object.clients.all() and parent_project and parent_project.clients.all():
                if options['verbosity'] > 0:
                    print(
                        'Setting clients of',
                        project_name,
                        'to clients of',
                        parent_project.name,
                        ':',
                        parent_project.clients.all(),
                    )
                project_object.clients.set(parent_project.clients.all())

            if set(project_object.cpfs.all()) != cpfs:
                if options['verbosity'] > 0:
                    print('Update cpfs of project', project_name, '#', project_id)
                project_object.cpfs.set(cpfs)

        # Purge dead projects
        projects.exclude(id__in=alive_projects).delete()
