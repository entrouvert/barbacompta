# Generated by Django 2.2.9 on 2020-03-16 15:16

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0009_client_chorus_structure'),
    ]

    operations = [
        migrations.AddField(
            model_name='facture',
            name='private_notes',
            field=models.TextField(
                blank=True,
                help_text='À usage purement interne, ne seront jamais présentes sur la facture',
                verbose_name='Notes privées',
            ),
        ),
    ]
