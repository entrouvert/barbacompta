# barbacompta - invoicing for dummies
# Copyright (C) 2010-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import date, timedelta

from django.contrib.auth import get_user_model

from eo_gestion.eo_facture.forms import FactureForm
from eo_gestion.eo_facture.models import Client, Contrat

User = get_user_model()


def test_facture_form(db, freezer):
    freezer.move_to('2019-01-01')

    creator = User.objects.create(username='user')
    data = {
        'proforma': 'true',
        'intitule': 'facture pour truc',
        'emission': '2019-01-01',
        'accounting_year': '2019',
        'echeance': '2019-01-30',
        'taux_tva': '20',
        'sous_traite': '0',
        'creator': creator.id,
    }

    # assert client or contract is provided
    form = FactureForm(data=data)
    assert not form.is_valid()
    client = Client()
    client.save()
    data['client'] = client.id
    form = FactureForm(data=data)
    assert form.is_valid(), form.errors
    contrat = Contrat(client=client, intitule='contrat pour des trucs', creator=creator)
    contrat.save()
    data['contrat'] = contrat.id
    form = FactureForm(data=data)
    assert form.is_valid(), form.errors

    facture = form.save()
    assert facture.id is not None
    assert facture.proforma
    assert facture.ordre is None
    assert facture.emission == date(2019, 1, 1)
    assert facture.echeance == date(2019, 1, 30)

    data['initial-emission'] = '2019-01-01'

    freezer.move_to('2019-01-02')
    form = FactureForm(data=dict(data, proforma='false'), instance=facture)
    assert form.is_valid(), form.errors
    facture = form.save()
    assert not facture.proforma
    assert facture.ordre is not None
    assert facture.emission == date(2019, 1, 2)
    assert facture.echeance == date(2019, 1, 2) + timedelta(days=45)

    # try setting proforma to False and changing emission
    facture.proforma = True
    facture.ordre = None
    facture.save()

    data['echeance'] = '2019-02-16'
    data['initial-echeance'] = '2019-02-16'
    form = FactureForm(data=dict(data, proforma='false', emission='2019-01-03'), instance=facture)
    assert form.is_valid(), form.errors
    facture = form.save()
    assert not facture.proforma
    assert facture.ordre is not None
    assert facture.emission == date(2019, 1, 3)
    assert facture.echeance == date(2019, 1, 3) + timedelta(days=45)

    data['proforma'] = 'false'
    data['initial-emission'] = '2019-01-03'
    form = FactureForm(data=dict(data, proforma='true', emission='2019-01-01'), instance=facture)
    assert not form.is_valid(), form.errors
    assert set(form.errors) == {'proforma', 'emission'}
