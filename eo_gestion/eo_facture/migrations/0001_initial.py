from decimal import Decimal

import django.core.validators
from django.conf import settings
from django.db import migrations, models

import eo_gestion.eo_facture.fields
import eo_gestion.eo_facture.models


class Migration(migrations.Migration):
    dependencies = [
        ('eo_banque', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('nom', models.CharField(unique=True, max_length=255)),
                ('adresse', models.TextField()),
                (
                    'email',
                    models.CharField(
                        blank=True,
                        max_length=50,
                        verbose_name='Courriel',
                        validators=[django.core.validators.EmailValidator()],
                    ),
                ),
                (
                    'telephone',
                    models.CharField(
                        blank=True,
                        max_length=20,
                        verbose_name='Téléphone',
                        validators=[django.core.validators.RegexValidator('[. 0-9]*')],
                    ),
                ),
                ('monnaie', models.CharField(default='\u20ac', max_length=10)),
                (
                    'tva',
                    models.DecimalField(
                        default=Decimal('20'),
                        verbose_name='TVA par défaut',
                        max_digits=8,
                        decimal_places=2,
                    ),
                ),
            ],
            options={
                'ordering': ('nom',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contrat',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('intitule', models.CharField(max_length=150)),
                ('description', models.TextField(blank=True)),
                ('tva', models.DecimalField(default=Decimal('20'), max_digits=8, decimal_places=2)),
                ('creation', models.DateField(default=eo_gestion.eo_facture.models.today)),
                (
                    'percentage_per_year',
                    eo_gestion.eo_facture.fields.PercentagePerYearField(
                        default=eo_gestion.eo_facture.models.one_hundred_percent_this_year,
                        help_text='Incompatible avec la périodicité',
                        max_length=64,
                        verbose_name='Pourcentage par année',
                    ),
                ),
                (
                    'montant_sous_traite',
                    models.DecimalField(default=Decimal('0'), max_digits=8, decimal_places=2),
                ),
                (
                    'client',
                    models.ForeignKey(
                        related_name='contrats', to='eo_facture.Client', on_delete=models.CASCADE
                    ),
                ),
                (
                    'creator',
                    models.ForeignKey(
                        verbose_name='Créateur', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('-id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Facture',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'proforma',
                    models.BooleanField(default=True, db_index=True, verbose_name='Devis'),
                ),
                (
                    'ordre',
                    models.IntegerField(
                        verbose_name="Numéro de la facture dans l'année",
                        unique_for_year='emission',
                        null=True,
                        editable=False,
                        blank=True,
                    ),
                ),
                ('intitule', models.CharField(max_length=150, verbose_name='Intitul\xe9', blank=True)),
                ('notes', models.TextField(blank=True)),
                (
                    'emission',
                    models.DateField(
                        default=eo_gestion.eo_facture.models.today, verbose_name='\xc9mission', db_index=True
                    ),
                ),
                (
                    'echeance',
                    models.DateField(
                        default=eo_gestion.eo_facture.models.today_plus_delai,
                        verbose_name='\xc9ch\xe9ance (par d\xe9faut 45 jours)',
                    ),
                ),
                ('taux_tva', models.DecimalField(default=Decimal('20'), max_digits=8, decimal_places=2)),
                (
                    'sous_traite',
                    eo_gestion.eo_facture.fields.EuroField(
                        default=Decimal('0'),
                        help_text='indiquer une somme pas un pourcentage, hors-taxe',
                        verbose_name='Montant sous-trait\xe9',
                        max_digits=8,
                        decimal_places=2,
                    ),
                ),
                (
                    'paid',
                    models.BooleanField(blank=True, default=False, db_index=True, verbose_name='Soldée'),
                ),
                (
                    'client',
                    models.ForeignKey(
                        related_name='direct_factures',
                        blank=True,
                        to='eo_facture.Client',
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'contrat',
                    models.ForeignKey(
                        related_name='factures',
                        blank=True,
                        to='eo_facture.Contrat',
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'creator',
                    models.ForeignKey(
                        verbose_name='Cr\xe9ateur', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('-id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ligne',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('intitule', models.TextField(blank=True)),
                (
                    'prix_unitaire_ht',
                    models.DecimalField(default=Decimal('0'), max_digits=8, decimal_places=2),
                ),
                ('quantite', models.DecimalField(default=Decimal('1.0'), max_digits=8, decimal_places=2)),
                ('taux_tva', models.DecimalField(null=True, max_digits=4, decimal_places=2, blank=True)),
                ('order', models.IntegerField(null=True, blank=True, default=0)),
                (
                    'facture',
                    models.ForeignKey(
                        related_name='lignes', to='eo_facture.Facture', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('order',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'montant_affecte',
                    models.DecimalField(
                        help_text="Si vide, le montant non affect\xe9 de l'encaissement est pris comme valeur",
                        verbose_name='Montant affect\xe9',
                        max_digits=8,
                        decimal_places=2,
                        blank=True,
                    ),
                ),
                (
                    'facture',
                    models.ForeignKey(
                        related_name='payments', to='eo_facture.Facture', on_delete=models.CASCADE
                    ),
                ),
                (
                    'ligne_banque_pop',
                    models.ForeignKey(
                        limit_choices_to={'montant__gt': 0},
                        related_name='payments',
                        verbose_name='Encaissement',
                        to='eo_banque.LigneBanquePop',
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                'ordering': ('-ligne_banque_pop__date_valeur',),
                'verbose_name': 'Paiement',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Prestation',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('intitule', models.CharField(max_length=255, verbose_name='Intitul\xe9')),
                ('optionnel', models.BooleanField(blank=True, default=False)),
                ('prix_unitaire_ht', models.DecimalField(max_digits=8, decimal_places=2)),
                ('quantite', models.DecimalField(max_digits=8, decimal_places=2)),
                (
                    'contrat',
                    models.ForeignKey(
                        related_name='prestations', to='eo_facture.Contrat', on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                'ordering': ('contrat', 'id'),
            },
            bases=(models.Model,),
        ),
    ]
