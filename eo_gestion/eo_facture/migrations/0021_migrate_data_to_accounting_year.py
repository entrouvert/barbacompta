# Generated by Django 3.2.23 on 2024-01-11 11:52

from django.db import migrations


def initialize_accounting_year(apps, schema_editor):
    Facture = apps.get_model('eo_facture', 'Facture')
    for facture in Facture.objects.filter(accounting_year=1):
        if facture.account_on_previous_period:
            facture.accounting_year = facture.emission.year - 1
        else:
            facture.accounting_year = facture.emission.year
        facture.save()


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0020_facture_accounting_year'),
    ]

    operations = [migrations.RunPython(initialize_accounting_year, migrations.RunPython.noop)]
