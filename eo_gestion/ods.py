# barbacompta - invoicing for dummies
# Copyright (C) 2005-2013  Entr'ouvert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import re
import xml.etree.ElementTree as ET
import zipfile
from datetime import date, datetime

from django.utils.encoding import force_str

NS = {
    'fo': 'urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0',
    'office': 'urn:oasis:names:tc:opendocument:xmlns:office:1.0',
    'style': 'urn:oasis:names:tc:opendocument:xmlns:style:1.0',
    'number': 'urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0',
    'table': 'urn:oasis:names:tc:opendocument:xmlns:table:1.0',
    'text': 'urn:oasis:names:tc:opendocument:xmlns:text:1.0',
    'xlink': 'http://www.w3.org/1999/xlink',
}

for prefix, uri in NS.items():
    ET.register_namespace(prefix, uri)


def is_number(value):
    if isinstance(value, str) and (value.startswith('0') or value.startswith('+')):
        return False
    try:
        float(value)
    except (ValueError, TypeError):
        return False
    return True


class Workbook:
    def __init__(self):
        self.sheets = []

    def add_sheet(self, name):
        sheet = WorkSheet(self, name)
        self.sheets.append(sheet)
        return sheet

    def get_content_node(self):
        root = ET.Element('{%s}document-content' % NS['office'])
        ET.SubElement(root, '{%s}scripts' % NS['office'])
        ET.SubElement(root, '{%s}font-face-decls' % NS['office'])

        body = ET.SubElement(root, '{%s}body' % NS['office'])
        spreadsheet = ET.SubElement(body, '{%s}spreadsheet' % NS['office'])
        for sheet in self.sheets:
            spreadsheet.append(sheet.get_node())
        return root

    def get_styles_node(self):
        root = ET.Element('{%s}document-styles' % NS['office'])
        ET.SubElement(root, '{%s}font-face-decls' % NS['office'])
        automatic_styles = ET.SubElement(root, '{%s}styles' % NS['office'])
        style = ET.SubElement(automatic_styles, '{%s}style' % NS['style'])
        style.attrib['{%s}name' % NS['style']] = 'Default'

        def define_number_style():
            node = ET.SubElement(automatic_styles, '{%s}number-style' % NS['number'])
            node.attrib['{%s}name' % NS['style']] = 'NumberFormat'
            sub_node = ET.SubElement(node, '{%s}number' % NS['number'])
            sub_node.attrib['{%s}decimal-places' % NS['number']] = '2'
            sub_node.attrib['{%s}min-decimal-places' % NS['number']] = '2'
            sub_node.attrib['{%s}min-integer-digits' % NS['number']] = '1'
            sub_node.attrib['{%s}grouping' % NS['number']] = 'true'
            style = ET.SubElement(automatic_styles, '{%s}style' % NS['style'])
            style.attrib['{%s}name' % NS['style']] = 'Number'
            style.attrib['{%s}family' % NS['style']] = 'table-cell'
            style.attrib['{%s}data-style-name' % NS['style']] = 'NumberFormat'
            style.attrib['{%s}parent-style' % NS['style']] = 'Default'

        define_number_style()

        def define_date_style(name, strftime_string):
            node = ET.SubElement(automatic_styles, '{%s}date-style' % NS['number'])
            node.attrib['{%s}name' % NS['style']] = name + 'NumberFormat'
            for part in re.findall(r'%?.', strftime_string):
                if part == '%Y':
                    ET.SubElement(node, '{%s}year' % NS['number']).attrib['{%s}style' % NS['number']] = 'long'
                elif part == '%m':
                    ET.SubElement(node, '{%s}month' % NS['number']).attrib[
                        '{%s}style' % NS['number']
                    ] = 'long'
                elif part == '%d':
                    ET.SubElement(node, '{%s}day' % NS['number']).attrib['{%s}style' % NS['number']] = 'long'
                elif part == '%H':
                    ET.SubElement(node, '{%s}hours' % NS['number']).attrib[
                        '{%s}style' % NS['number']
                    ] = 'long'
                elif part == '%M':
                    ET.SubElement(node, '{%s}minutes' % NS['number']).attrib[
                        '{%s}style' % NS['number']
                    ] = 'long'
                elif part == '%S':
                    ET.SubElement(node, '{%s}seconds' % NS['number']).attrib[
                        '{%s}style' % NS['number']
                    ] = 'long'
                else:
                    ET.SubElement(node, '{%s}text' % NS['number']).text = part
            style = ET.SubElement(automatic_styles, '{%s}style' % NS['style'])
            style.attrib['{%s}name' % NS['style']] = name
            style.attrib['{%s}family' % NS['style']] = 'table-cell'
            style.attrib['{%s}data-style-name' % NS['style']] = name + 'NumberFormat'
            style.attrib['{%s}parent-style' % NS['style']] = 'Default'

            style = ET.SubElement(automatic_styles, '{%s}style' % NS['style'])
            style.attrib['{%s}name' % NS['style']] = name + 'Column'
            style.attrib['{%s}family' % NS['style']] = 'table-column'
            ET.SubElement(style, '{%s}table-column-properties' % NS['style']).attrib[
                '{%s}column-width' % NS['style']
            ] = '80mm'

        define_date_style('Date', '%d/%m/%Y')
        define_date_style('DateTime', '%d/%m/%Y %H:%M')

        return root

    def get_styles(self):
        return ET.tostring(self.get_styles_node(), 'utf-8')

    def get_content(self):
        return ET.tostring(self.get_content_node(), 'utf-8')

    def save(self, output):
        with zipfile.ZipFile(output, 'w') as z:
            z.writestr('content.xml', self.get_content())
            z.writestr('styles.xml', self.get_styles())
            z.writestr('mimetype', 'application/vnd.oasis.opendocument.spreadsheet')
            z.writestr(
                'META-INF/manifest.xml',
                """<?xml version="1.0" encoding="UTF-8"?>
    <manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0">
     <manifest:file-entry manifest:full-path="/" manifest:media-type="application/vnd.oasis.opendocument.spreadsheet"/>
     <manifest:file-entry manifest:full-path="styles.xml" manifest:media-type="text/xml"/>
     <manifest:file-entry manifest:full-path="content.xml" manifest:media-type="text/xml"/>
     <manifest:file-entry manifest:full-path="META-INF/manifest.xml" manifest:media-type="text/xml"/>
     <manifest:file-entry manifest:full-path="mimetype" manifest:media-type="text/plain"/>
    </manifest:manifest>""",
            )


class WorkSheet:
    def __init__(self, workbook, name):
        self.cells = {}
        self.name = name
        self.workbook = workbook

    def write(self, row, column, value, **kwargs):
        if row not in self.cells:
            self.cells[row] = {}
        self.cells[row][column] = WorkCell(self, value, **kwargs)

    def get_node(self):
        root = ET.Element('{%s}table' % NS['table'])
        root.attrib['{%s}name' % NS['table']] = self.name
        ET.SubElement(root, '{%s}table-column' % NS['table'])
        for i in range(0, max(self.cells.keys()) + 1):
            row = ET.SubElement(root, '{%s}table-row' % NS['table'])
            for j in range(0, max(self.cells.get(i).keys()) + 1):
                cell = self.cells.get(i, {}).get(j, None)
                if not cell:
                    ET.SubElement(row, '{%s}table-cell' % NS['table'])
                else:
                    row.append(cell.get_node())
        return root


class WorkCell:
    def __init__(self, worksheet, value):
        if value is None:
            value = ''
        self.worksheet = worksheet
        self.native_value = value
        value = force_str(value)
        for i in range(0x20):  # remove control characters
            char = chr(i)
            if char in ('\t', '\r', '\n'):
                # only allow tab, carriage return and line feed.
                continue
            value = value.replace(char, '')
        # fffe and ffff are also invalid characters
        value = value.replace('\ufffe', '').replace('\uffff', '')
        self.value = value

    def get_node(self):
        root = ET.Element('{%s}table-cell' % NS['table'])
        p = ET.SubElement(root, '{%s}p' % NS['text'])
        p.text = self.value
        value = self.native_value
        if is_number(value):
            root.attrib['{%s}value-type' % NS['office']] = 'float'
            root.attrib['{%s}value' % NS['office']] = self.value
            root.attrib['{%s}style-name' % NS['table']] = 'Number'
        elif isinstance(value, datetime):
            root.attrib['{%s}style-name' % NS['table']] = 'DateTime'
            root.attrib['{%s}value-type' % NS['office']] = 'date'
            root.attrib['{%s}date-value' % NS['office']] = value.strftime('%Y-%m-%dT%H:%M:%S')
            p.text = value.strftime('%d/%m/%Y %H:%M:%S')
        elif isinstance(value, date):
            root.attrib['{%s}style-name' % NS['table']] = 'Date'
            root.attrib['{%s}value-type' % NS['office']] = 'date'
            root.attrib['{%s}date-value' % NS['office']] = value.strftime('%Y-%m-%d')
            p.text = value.strftime('%d/%m/%Y')
        else:
            root.attrib['{%s}value-type' % NS['office']] = 'string'
        return root
