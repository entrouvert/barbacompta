# Generated by Django 2.2.9 on 2020-02-01 14:59

from django.db import migrations, models

import eo_gestion.eo_facture.validators


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0007_auto_20191009_1335'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='service_code',
            field=models.CharField(blank=True, db_index=True, default='', max_length=128),
        ),
        migrations.AddField(
            model_name='client',
            name='siret',
            field=models.CharField(
                blank=True,
                db_index=True,
                default='',
                max_length=14,
                validators=[eo_gestion.eo_facture.validators.validate_siret],
            ),
        ),
        migrations.AddField(
            model_name='contrat',
            name='numero_marche',
            field=models.CharField(blank=True, default='', max_length=128, verbose_name='Numéro du marché'),
        ),
        migrations.AddField(
            model_name='facture',
            name='numero_engagement',
            field=models.CharField(
                blank=True, default='', max_length=128, verbose_name="Numéro d'engagement"
            ),
        ),
    ]
