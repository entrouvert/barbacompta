# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from decimal import Decimal

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0006_client_contacts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ligne',
            name='quantite',
            field=models.DecimalField(decimal_places=3, default=Decimal('1.0'), max_digits=8),
        ),
        migrations.AlterField(
            model_name='prestation',
            name='quantite',
            field=models.DecimalField(decimal_places=3, max_digits=8),
        ),
    ]
