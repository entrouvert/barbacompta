# barbacompta - invoicing for dummies
# Copyright (C) 2019-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from taggit.models import Tag

from eo_gestion.eo_facture.models import Contrat


def test_references(app):
    gru = Tag.objects.create(name='GRU')
    gi = Tag.objects.create(name='Gestion d\'identité')

    for i, contract in enumerate(Contrat.objects.all()):
        if i % 3 == 0:
            contract.tags.add(gru)
        if i % 3 == 1:
            contract.tags.add(gi)

    response = app.get('/api/references/')
    assert any(row['tags'] for row in response.json['data'])
    response_gru = app.get('/api/references/', params={'tags': 'GRU'})
    assert len(response_gru.json['data']) == 12
    response_gi = app.get('/api/references/', params={'tags': 'Gestion d\'identité'})
    assert len(response_gi.json['data']) == 18
    response_gru_and_gi = app.get('/api/references/', params={'tags': ['GRU', 'Gestion d\'identité']})
    assert len(response_gru_and_gi.json['data']) == 30
