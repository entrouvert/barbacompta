from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='facture',
            name='account_on_previous_period',
            field=models.BooleanField(
                default=False, verbose_name="Mettre cette facture sur l'exercice pr\xe9c\xe9dent"
            ),
            preserve_default=True,
        ),
    ]
