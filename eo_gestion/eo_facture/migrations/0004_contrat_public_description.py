from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('eo_facture', '0003_client_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='contrat',
            name='public_description',
            field=models.TextField(
                verbose_name='Description publique',
                blank=True,
                help_text='Si elle est présente, cette description sera reprise dans la liste des références sur le site web.',
            ),
        ),
    ]
