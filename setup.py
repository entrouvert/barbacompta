#! /usr/bin/env python


import os
import subprocess

try:
    from setuptools import Command
    from setuptools.command.build import build as _build
except ImportError:
    from distutils.cmd import Command
    from distutils.command.build import build as _build

from setuptools import find_packages, setup
from setuptools.command.install_lib import install_lib as _install_lib
from setuptools.command.sdist import sdist as sdist

install_requires = [
    'django>=3.2,<3.3',
    'weasyprint<0.43',
    'django-model-utils<4',
    'factur-x',
    'requests',
    'django-taggit==2.1.0',  # 2.1.0 is available in bullseye
    'XStatic-Select2',
    'gadjo',
    'django-mellon',
    'python-dateutil',
    'caldav',
    'setuptools',  # facturx use pkg_resources :/
    'pypdf',
]


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result.replace('.dirty', '+dirty')
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        from django.core.management import call_command

        os.environ.pop('DJANGO_SETTINGS_MODULE', None)
        for path, dirs, files in os.walk('eo_gestion'):
            if 'locale' not in dirs:
                continue
            curdir = os.getcwd()
            os.chdir(os.path.realpath(path))
            call_command('compilemessages')
            os.chdir(curdir)


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


setup(
    name='barbacompta',
    version=get_version(),
    license='AGPLv3 or later',
    description='Logiciel de compta/facturation',
    url='http://dev.entrouvert.org/projects/gestion',
    author='Entr\'ouvert',
    author_email='info@entrouvert.org',
    maintainer='Benjamin Dauvergne',
    maintainer_email='bdauvergne@entrouvert.com',
    include_package_data=True,
    scripts=['manage.py'],
    packages=find_packages(),
    install_requires=install_requires,
    setup_requires=[
        'django>=3.2,<3.3',
    ],
    cmdclass={
        'build': build,
        'compile_translations': compile_translations,
        'install_lib': install_lib,
        'sdist': eo_sdist,
    },
)
